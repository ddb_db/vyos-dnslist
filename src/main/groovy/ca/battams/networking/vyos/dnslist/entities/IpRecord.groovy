/*
    vyos-dnslist: A DNS based address list manager for VyOS

    Copyright (C) 2021 Battams, Derek <derek@battams.ca>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package ca.battams.networking.vyos.dnslist.entities

import groovy.transform.EqualsAndHashCode

import javax.persistence.*
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

@Entity
@EqualsAndHashCode(includes = ['ip'])
class IpRecord {
    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE) Long id
    @ManyToOne(cascade = CascadeType.ALL, optional = false) FQDN domain
    @NotEmpty String ip
    @NotNull Date expires
}
