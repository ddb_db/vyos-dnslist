/*
    vyos-dnslist: A DNS based address list manager for VyOS

    Copyright (C) 2021 Battams, Derek <derek@battams.ca>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package ca.battams.networking.vyos.dnslist

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
class DnslistApplication {

	static void main(String[] args) {
		def app = new SpringApplication(DnslistApplication)
		app.setDefaultProperties([
				'spring.datasource.url': 'jdbc:h2:mem:vyos_dnslist',
				'spring.datasource.driverClassName': 'org.h2.Driver',
				'spring.datasource.username': 'sa',
				'spring.datasource.password': 'password',
				'spring.jpa.database-platform': 'org.hibernate.dialect.H2Dialect'
		])
		SpringApplication.run(DnslistApplication, args)
	}
}
