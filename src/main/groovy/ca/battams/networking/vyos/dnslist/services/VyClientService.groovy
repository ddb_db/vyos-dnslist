/*
    vyos-dnslist: A DNS based address list manager for VyOS

    Copyright (C) 2021 Battams, Derek <derek@battams.ca>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package ca.battams.networking.vyos.dnslist.services

import ca.battams.networking.vyos.dnslist.config.AppConfig
import groovy.json.JsonOutput
import groovy.util.logging.Slf4j
import groovyx.net.http.RESTClient
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
@Slf4j
class VyClientService {

    private final AppConfig appConfig
    private final RESTClient http
    private final String apiKey

    @Autowired
    VyClientService(AppConfig appConfig) {
        http = new RESTClient(appConfig.vyosUrl)
        if(appConfig.ignoreSsl)
            http.ignoreSSLIssues()
        http.defaultContentType = 'application/json'
        http.defaultRequestContentType = 'application/x-www-form-urlencoded'
        apiKey = appConfig.apiKey
        this.appConfig = appConfig
    }

    void setAddressGroups(Map lists) {
        def commands = []
        lists.each { name, ips ->
            commands << [op: 'delete', path: ['firewall', 'group', 'address-group', name]]
            ips.each {
                commands << [op: 'set', path: ['firewall', 'group', 'address-group', name, 'address', it]]
            }
        }
        execute(commands)
    }

    void setPortGroups(Map lists) {
        def natRuleId = appConfig.natRule
        def commands = []
        lists.each { name, ports ->
            commands << [op: 'delete', path: ['firewall', 'group', 'port-group', name]]
            if(natRuleId)
                commands << [op: 'delete', path: ['nat', 'destination', 'rule', natRuleId, 'destination', 'port']]
            ports.each {
                commands << [op: 'set', path: ['firewall', 'group', 'port-group', name, 'port', it.toString()]]
                if(natRuleId)
                    commands << [op: 'set', path: ['nat', 'destination', 'rule', natRuleId, 'destination', 'port', it.toString()]]
            }
        }
        execute(commands)
    }

    private void execute(List commands) {
        if(!commands) return
        if(log.isInfoEnabled()) {
            def cmdLog = new StringBuilder('Executing commands:\n')
            commands.collect { "$it.op ${it.path.join(' ')}"}.each {
                cmdLog << "$it\n"
            }
            log.info cmdLog.toString()
        }
        def result = http.post(path: '/configure', body: [key: apiKey, data: JsonOutput.toJson(commands)]).data
        if(!result.success || result.error)
            throw new IOException("VyOS update failed: $result.error")
    }
}
