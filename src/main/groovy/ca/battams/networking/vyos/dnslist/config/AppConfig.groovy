/*
    vyos-dnslist: A DNS based address list manager for VyOS

    Copyright (C) 2021 Battams, Derek <derek@battams.ca>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package ca.battams.networking.vyos.dnslist.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration
import org.springframework.validation.annotation.Validated

import javax.validation.constraints.NotEmpty

@Configuration
@ConfigurationProperties(prefix = 'dnslist')
@Validated
class AppConfig {
    File addressJson = new File('addresses.json')
    File portsJson = new File('ports.json')
    @NotEmpty String vyosUrl
    @NotEmpty String apiKey
    @NotEmpty String queryFreq = 'PT10M'
    // TODO: This should be a nat rule per list, not a global setting
    String natRule
    boolean ignoreSsl = false
    String healthUrl
}
