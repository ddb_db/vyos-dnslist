/*
    vyos-dnslist: A DNS based address list manager for VyOS

    Copyright (C) 2021 Battams, Derek <derek@battams.ca>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package ca.battams.networking.vyos.dnslist.tasks

import ca.battams.networking.vyos.dnslist.config.AppConfig
import ca.battams.networking.vyos.dnslist.entities.AddressList
import ca.battams.networking.vyos.dnslist.entities.FQDN
import ca.battams.networking.vyos.dnslist.entities.IpRecord
import ca.battams.networking.vyos.dnslist.repos.AddressListRepo
import ca.battams.networking.vyos.dnslist.repos.FQDNRepo
import ca.battams.networking.vyos.dnslist.services.VyClientService
import groovy.json.JsonSlurper
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import org.xbill.DNS.Lookup
import org.xbill.DNS.Type

import javax.transaction.Transactional

@Slf4j
@Component
class ScheduledTasks {

    @Autowired FQDNRepo fqdnRepo
    @Autowired AddressListRepo addressListRepo
    @Autowired VyClientService vyos
    @Autowired AppConfig appConfig

    private boolean firstRunCompleted = false
    private Map portCache = [:]

    @Scheduled(fixedDelayString = '#{@appConfig.queryFreq}')
    @Transactional
    void update() {
        try {
            queryDnsAndUpdateAddressLists()
            updatePortLists()
            sendSignal()
        } catch(Throwable t) {
            log.error 'Updates failed', t
            sendSignal(1)
            throw t
        }
    }

    private void sendSignal(int rc = 0) {
        if(appConfig.healthUrl)
            new URL("${appConfig.healthUrl}/$rc").text
    }

    private void queryDnsAndUpdateAddressLists() {
        if(!appConfig.addressJson.exists()) {
            log.warn "$appConfig.addressJson does not exist; skipping"
            return
        }
        def json = new JsonSlurper().parse(appConfig.addressJson)
        def lists = [:]
        json.keySet().each { listName ->
            def addressList = addressListRepo.findById(listName).orElseGet({ -> new AddressList(name: listName) })
            def updatedIps = new HashSet()
            json[listName].each { domain ->
                def now = new Date()
                def fqdn = fqdnRepo.findById(domain).orElseGet({ -> new FQDN(name: domain) })
                new Lookup(domain, Type.A).run().each {
                    def ip = it.address.hostAddress
                    def record = fqdn.ipRecords.find { it.ip == ip }
                    if(!record)
                        fqdn.ipRecords << new IpRecord(ip: ip, expires: new Date(System.currentTimeMillis() + (1000L * it.ttl)), domain: fqdn)
                    else if(record.expires.before(now))
                        record.expires = new Date(System.currentTimeMillis() + (1000L * it.ttl))
                }
                def itr = fqdn.ipRecords.iterator()
                while(itr.hasNext()) {
                    def it = itr.next()
                    if(it.expires.before(now))
                        itr.remove()
                }
                fqdnRepo.save(fqdn)
                updatedIps.addAll(fqdn.ipRecords.collect { it.ip })
            }
            if(!firstRunCompleted || addressList.ips.sort() != updatedIps.sort()) {
                def list = lists[listName]
                if(!list) {
                    list = []
                    lists[listName] = list
                }
                list.addAll(updatedIps)
                addressList.ips.clear()
                addressList.ips.addAll(list)
                addressListRepo.saveAndFlush(addressList)
            } else
                log.debug "IP list for $listName did not change, ignoring"
        }
        firstRunCompleted = true
        if(lists)
            vyos.setAddressGroups(lists)
        else
            log.debug 'No address lists are defined or no changes detected'
    }

    private void updatePortLists() {
        if(!appConfig.portsJson.exists()) {
            log.warn "$appConfig.portsJson does not exist; skipping"
            return
        }
        def json = new JsonSlurper().parse(appConfig.portsJson)
        def lists = [:]
        json.keySet().each { listName ->
            def cachedList = portCache[listName]
            def currentList = []
            json[listName].each { url ->
                currentList.addAll(new URL(url).text.split('\n').toList())
            }
            if(!cachedList || currentList.sort() != cachedList.sort()) {
                portCache[listName] = currentList
                lists[listName] = currentList
            } else
                log.debug "Port list for $listName did not change, ignoring"
        }
        if(lists)
            vyos.setPortGroups(lists)
        else
            log.debug 'No port lists are defined or no changes detected'
    }
}
